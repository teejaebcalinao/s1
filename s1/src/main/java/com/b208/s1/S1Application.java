package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web requests.
public class S1Application {
	//Annotations in Java Springboot marks classes for their intended functionalities.
	//Springboot upon startup scans for classes and assign behaviors based on their annotation.
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	//@GetMapping will map a route or an endpoint to access or run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to run the hello() method.
	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John") String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//WE retrieve the value of the filed "name" from our URL
		//defaultValue is the fallback value when the query string or request param is empty.
		//System.out.println(nameParameter);
		return "Hi! My name is " + nameParameter;
	}

	//Create a new method called myFavoriteFood()
	//Map an endpoint to the method using @GetMapping()
		//Method receives data as RequestParam and value is food
		//Add an appropriate String parameter
		//return the message to the client:
		//"Hi! My favorite food is <valueFood>"
	@GetMapping("/favoriteFood")
	public String myFavoriteFood(@RequestParam(value="food",defaultValue = "rice") String foodParams){
		return "Hi! My favorite food is " + foodParams;
	}
	//2 ways of passing data through the URL by using Java Springboot.
	//Query String using Request Params - Directly passing data into the url and getting the data
	//Path Variable is much more similar to ExpressJS's req.params
	@GetMapping("/greeting/{element}")
	public String greeting(@PathVariable("element") String nameParams){
		//@PathVariable() allows us to extract data directly from the url.
		return "Hello " + nameParams;

	}
}
